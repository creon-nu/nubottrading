##Available price feed names (can be used as value for *main-feed* and *backup-feeds*)

| Feed name        | Currencies available for tracking   |  Feed |   
| ------------- |:-------------:| -------------:| 
| blockchain    | BTC  |  https://blockchain.info |
| bitcoinaverage    | BTC  |  https://api.bitcoinaverage.com |
| coinbase    | BTC  |  https://coinbase.com |
| bitstamp    | BTC  |  https://bitstamp.com |
| bitfinex    | BTC  |  https://api.bitfinex.com |
| btce  | BTC, PPC, ...  |  https://btc-e.com |
| bter    | BTC  |  http://data.bter.com |
| ccedk    | BTC |  https://www.ccedk.com |
| coinmarketcap_no    | PPC |  http://coinmarketcap.northpole.ro |
| coinmarketcap_ne    | PPC |  http://coinmarketcap-nexuist.rhcloud.com |
| bitstampeurusd    | EUR  |  https://bitstamp.com |
| google-unofficial    | EUR,CNY,PHP,HKD ...  |  http://rate-exchange.appspot.com |
| yahoo    | EUR,CNY,PHP,HKD ...  |  https://yahooapis.com |
| openexchangerates    | EUR,CNY, PHP,HKD...  |  https://openexchangerates.org |
| exchangeratelab    | EUR,CNY, ...  |  https://exchangeratelab.com